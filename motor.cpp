#include "motor.h"
#include "gpio.h"

#define PI 3.14
//Board addresses definition
#define MOTOR_1_2_ADDR  	(0x35)          //Attiny2313 motor board address, main motor driver
#define MOTOR1_ADDR             (0xb1)          //Motor 1 buffer address
#define MOTOR2_ADDR             (0xb2)          //Motor 2 buffer address
#define TUR_MOT_ADDR	   	(0xb3)		//Turret motor address
#define GUN_MOT_ADDR	   	(0xb4)		//Gun motor address

Motor::Motor(){
	
	i2cbus = "/dev/i2c-1";
	gunPin = 7;
	setup_io();

}

int Motor::motorUpdateCmd(char cmd[15]){

	/**********Parse command**********/

	char cX[2] , cY[2];
	int x, y, angle, motExt, motInt;

	cX[0] = cmd[1];
	cX[1] = cmd[2];
	if( cmd[0] == '-' ){
		x = atoi(cX) * -1;
	}else{
		x = atoi(cX);
	}
	
	cY[0] = cmd[4];
	cY[1] = cmd[5];
	if( cmd[3] == '-' ){
		y = atoi(cX) * -1;
	}else{
		y = atoi(cX);
	}

	motExt = sqrt((x*x)+(y*y)) * 255 / 100;

	angle = (int) atan2(y,x) * 180 / PI ;

	motInt = abs(angle * 255 / 90) ;

        if( x<0 & y>=0){
		i2c_write(MOTOR1_ADDR, 1, motExt); //motor1
		i2c_write(MOTOR2_ADDR, 1, motInt); //motor2
        }
        else if( x<0 & y<0){
		i2c_write(MOTOR1_ADDR, 0, motExt); //motor1
		i2c_write(MOTOR2_ADDR, 0, motInt); //motor2
        }
        else if( x>=0 & y<0 ){
		i2c_write(MOTOR1_ADDR, 0, motInt); //motor1
		i2c_write(MOTOR2_ADDR, 0, motExt); //motor2
        }
        else if( x>=0 & y>=0 ){
		i2c_write(MOTOR1_ADDR, 1, motInt); //motor1
		i2c_write(MOTOR2_ADDR, 1, motExt); //motor2
        }	
	
	switch(cmd[6])
        {
                case '1':
			i2c_write(GUN_MOT_ADDR, 1 , 0 );
	        break;

        	case '2':
			i2c_write(GUN_MOT_ADDR, 1 , 0 );
        	break;
	
        	case '3':
			i2c_write(TUR_MOT_ADDR, 0 , 255 );
        	break;

        	case '4':
			i2c_write(TUR_MOT_ADDR, 1 , 255 );
        	break;
	
        	default :
			i2c_write(TUR_MOT_ADDR, 0 , 0 );
			i2c_write(GUN_MOT_ADDR, 0 , 0 );
        	break;
        }
	
	if(cmd[7] == '5'){
		gpio_state(gunPin,1);
	}
	else{
		gpio_state(gunPin,0);
	}

	return 0;

}


int Motor::i2c_write(int addr, int dir, int speed){

        int fd;

	char buf[3];

        if ((fd = open(i2cbus, O_RDWR))<0){
                printf("Failed to open i2c port\n");
                return -1;
        }

        if (ioctl(fd, I2C_SLAVE, MOTOR_1_2_ADDR )<0){
                printf("Unable to get bus access to Motor Board\n");
                return -1;
	}

	buf[0] = addr;
	buf[1] = dir;
	buf[2] = speed;

       	write(fd, buf, 3);

        close(fd);

	return 0;

}


