#include <stdio.h>
#include <stdlib.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <pthread.h>
#include <syslog.h>
#include <signal.h>
#include <cmath>
#include <sys/mman.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstdlib>
#include <gst/gst.h>

#define BCM2708_PERI_BASE        0x20000000
#define GPIO_BASE                (BCM2708_PERI_BASE + 0x200000) /* GPIO controller */
#define PAGE_SIZE (4*1024)
#define BLOCK_SIZE (4*1024)

int  mem_fd;
void *gpio_map;
volatile unsigned *gpio;

// GPIO setup macros. Always use INP_GPIO(x) before using OUT_GPIO(x) or SET_GPIO_ALT(x,y)
#define INP_GPIO(g) *(gpio+((g)/10)) &= ~(7<<(((g)%10)*3))
#define OUT_GPIO(g) *(gpio+((g)/10)) |=  (1<<(((g)%10)*3))

#define GPIO_SET *(gpio+7) // sets bits which are 1 ignores bits which are 0
#define GPIO_CLR *(gpio+10) // clears bits which are 1 ignores bits which are 0

//Board buffer definition
#define MOTOR_1_2_ADDR  	(0x35)          //Attiny2313 motor board address, main motor driver
#define MOTOR1_ADDR             (0xb1)          //Motor 1 buffer address
#define MOTOR2_ADDR             (0xb2)          //Motor 2 buffer address
#define TUR_MOT_ADDR	   	(0xb3)
#define GUN_MOT_ADDR	   	(0xb4)
/*
*       Main motor driver data buffer:
*       SLAVE_ADDR
*       REGISTER_ADDR   	//Select the motor
*       DIRECTION		// 0~1
*       SPEED_VALUE             //0~255 , 8bit timer0 fast pwm value
*/

using namespace std;

char cmd[15];

int  dir1 , dir2 , speed1 , speed2 , deltaX , deltaY , deltaX_old , deltaY_old ;

bool isRecording;

string ip;

GstElement *pipeline, *source, *capsfilter, *rtph264, *sink, *vid_tee, *q1, *q2, *encoder, *mp4mux, *filesink;
GstPadTemplate *tee_src_pad_template;
GstPad *tee_q1_pad, *tee_q2_pad;
GstPad *q1_pad, *q2_pad;

/***************************************************************/

const char *i2cFileName = "/dev/i2c-1";    // I2C file descriptor

void i2c_write(int address , int direction , int speed)
{

        int fd;

	char buf[4];

        if ((fd = open(i2cFileName, O_RDWR))<0){
                printf("Failed to open i2c port\n");
                exit(1) ;
        }

        if (ioctl(fd, I2C_SLAVE, MOTOR_1_2_ADDR )<0){
                printf("Unable to get bus access to Motor Board\n");
                exit(1) ;
	}

	buf[0] = address;
	buf[1] = direction;
	buf[2] = speed;

       	write(fd, buf, 3);


        close(fd);
}


void motorControl()
{

        int mot_ext , mot_int ,rl_ext_mot ,rl_int_mot ;
        char x[2] , y[2] ;


	y[0]=cmd[4];
        y[1]=cmd[5];
	if(deltaY_old == NULL){
	        deltaY_old = atoi(y);
        	if(cmd[3]=='-'){
		        deltaY_old=-deltaY_old;
       		}
	}else{
		deltaY_old = deltaY;
	}

	deltaY = atoi(y);
       	if(cmd[3]=='-'){
	        deltaY=-deltaY;
 	}		

        x[0]=cmd[1];
        x[1]=cmd[2];
	if(deltaX_old == NULL){
	       	deltaX_old = atoi(x);
	        if(cmd[0]=='-'){
	     		deltaX_old=-deltaX_old;
	        }
	}else{
		deltaX_old = deltaX;
	}

	deltaX = atoi(x);
       	if(cmd[0]=='-'){
	        deltaX=-deltaX;
      	}		


        mot_ext = sqrt((deltaY*deltaY)+(deltaX*deltaX));

        if(mot_ext>= 99){
        
		mot_ext = 99;

        }

        mot_int= (mot_ext - fabs(deltaX));

        rl_ext_mot= ((mot_ext*255)/100);
        rl_int_mot= ((mot_int*255)/100);


        if( deltaX<0 & deltaY>=0){
        	dir1 = 1;
        	dir2 = 1;
	        speed2 = rl_int_mot;
        	speed1 = rl_ext_mot;
        }
        else if( deltaX<0 & deltaY<0){
                speed2 = rl_int_mot ;
                speed1 = rl_ext_mot ;
                dir1 = 0;
                dir2 = 0;
        }
        else if( deltaX>=0 & deltaY<0 ){
                speed2 = rl_ext_mot;
                speed1 = rl_int_mot;
                dir1 = 0;
                speed1 = rl_int_mot;
                dir1 = 0;
                dir2 = 0;
        }
        else if( deltaX>=0 & deltaY>=0 ){
                speed2 = rl_ext_mot;
                speed1 = rl_int_mot;
                dir1 = 1;
                dir2 = 1;
        }			

	switch(cmd[6])
        {
                case '1':
			i2c_write(GUN_MOT_ADDR, 1 , 0 );
	        break;

        	case '2':
			i2c_write(GUN_MOT_ADDR, 1 , 0 );
        	break;
	
        	case '3':
			i2c_write(TUR_MOT_ADDR, 0 , 255 );
        	break;

        	case '4':
			i2c_write(TUR_MOT_ADDR, 1 , 255 );
        	break;
	
        	default :
			i2c_write(TUR_MOT_ADDR, 0 , 0 );
			i2c_write(GUN_MOT_ADDR, 0 , 0 );
        	break;
        }

	if(( deltaX =! deltaX_old ) || ( deltaY =! deltaY_old )){ 
		i2c_write(MOTOR1_ADDR , dir1 , speed1 );
		i2c_write(MOTOR2_ADDR , dir2 , speed2 );
	}

	if(cmd[7] == '5'){
		GPIO_SET = 1<<27;
	}
	else{
		GPIO_CLR = 1<<27;
	}

}

void error(const char *msg)
{
	perror(msg);
	exit(1);
}

void record_start(string date){

	string ext = ".mp4";
	string name = date + ext; 

	g_object_set(filesink, "location", name.c_str() , NULL);

	tee_q2_pad = gst_element_request_pad (tee, tee_src_pad_template, NULL, NULL);
	g_print ("Obtained request pad %s for q2 branch.\n", gst_pad_get_name (tee_q2_pad));
	q2_pad = gst_element_get_static_pad (q2, "sink");

	/* Link the tee to the queue 2 */
	gst_pad_link (tee_q2_pad, q2_pad);

	gst_object_unref (q2_pad);

	gst_element_set_state(q2, GST_STATE_PLAYING);
	gst_element_set_state(h264parse2, GST_STATE_PLAYING);
	gst_element_set_state(mp4mux, GST_STATE_PLAYING);
	gst_element_set_state(filesink, GST_STATE_PLAYING);	

}

void record_stop(){

	gst_element_release_request_pad(vid_tee, q2_pad);

	gst_pad_unlink(tee_q2_pad, q2_pad);

	gst_element_send_event(q2 , gst_event_new_eos ());

	gst_element_set_state(q2, GST_STATE_NULL);
	gst_element_set_state(h264parse2, GST_STATE_NULL);
	gst_element_set_state(mp4mux, GST_STATE_NULL);
	gst_element_set_state(filesink, GST_STATE_NULL);	

}

void video_stop(){

	record_stop();
	
	gst_element_set_state (pipeline, GST_STATE_NULL);
	gst_object_unref(pipeline);

}	

void video_start(){
		
	gst_init (NULL,NULL);

	source = gst_element_factory_make("rpicamsrc","source");
	capsfilter = gst_element_factory_make("capsfilter","caps");
	h264parse = gst_element_factory_make("h264parse","h264parse");
	rtph264 = gst_element_factory_make("rtph264pay","rtph264");
	tee = gst_element_factory_make ("tee", "videotee");
	q1 = gst_element_factory_make ("queue", "qone");
	q2 = gst_element_factory_make ("queue", "qtwo");
	h264parse2 = gst_element_factory_make("h264parse","h264parse2");
	mp4mux = gst_element_factory_make("mp4mux","mux");
	sink = gst_element_factory_make("udpsink","sink");
	filesink = gst_element_factory_make ("filesink", "filesink");

	g_object_set(source, "bitrate", 6000000, NULL);
	g_object_set(rtph264, "config-interval", 1, NULL);
	g_object_set(rtph264, "pt" , 96, NULL);
	g_object_set(sink, "host", ip.c_str() , NULL);
	g_object_set(sink, "port", 5000, NULL);

	g_object_set(capsfilter, "caps", gst_caps_new_simple("video/x-h264",
		"width", G_TYPE_INT, 1280,
		"height", G_TYPE_INT, 720,
		"framerate", GST_TYPE_FRACTION, 30 , 1 ,
		"profile" ,G_TYPE_STRING, "main" ,NULL), NULL);

	pipeline = gst_pipeline_new("my_pipeline");
	gst_bin_add_many(GST_BIN(pipeline), source,capsfilter,rtph264,sink,vid_tee, q1, q2, filesink, mp4mux,NULL);

	pipeline = gst_pipeline_new("test-pipeline");

	gst_bin_add_many(GST_BIN(pipeline), source,capsfilter,h264parse,h264parse2,rtph264,sink,q1,q2,tee,filesink,mp4mux,NULL);
	

	gst_element_link_many(source,capsfilter,tee,NULL);
	gst_element_link_many(q1,h264parse,rtph264,sink,NULL);	
	gst_element_link_many(q2,h264parse2,mp4mux,filesink,NULL);

	/* Manually link the Tee, which has "Request" pads */
	tee_src_pad_template = gst_element_class_get_pad_template (GST_ELEMENT_GET_CLASS (tee), "src_%u");
 
	/* Obtaining request pads for the tee elements*/
	tee_q1_pad = gst_element_request_pad (tee, tee_src_pad_template, NULL, NULL);
	g_print ("Obtained request pad %s for q1 branch.\n", gst_pad_get_name (tee_q1_pad));
	q1_pad = gst_element_get_static_pad (q1, "sink");

	/* Link the tee to the queue 1 */
	gst_pad_link (tee_q1_pad, q1_pad);
	gst_object_unref (q1_pad);	

	gst_element_set_state (pipeline, GST_STATE_PLAYING);


}

void setup_io() { /* open /dev/mem */


	if ((mem_fd = open("/dev/mem", O_RDWR|O_SYNC) ) < 0) {
		printf("can't open /dev/mem \n");
		exit(-1);
	}

	/* mmap GPIO */
	gpio_map = mmap(
		NULL,                 //Any adddress in our space will do
		BLOCK_SIZE,           //Map length
		PROT_READ|PROT_WRITE, // Enable reading & writting to mapped memory
		MAP_SHARED,           //Shared with other processes
		mem_fd,               //File to map
		GPIO_BASE             //Offset to GPIO peripheral
	);

	close(mem_fd); //No need to keep mem_fd open after mmap

	if (gpio_map == MAP_FAILED) {
		//printf("mmap error %d\n", (int)gpio_map);//errno also set!
		exit(-1);
	}

	gpio = (volatile unsigned *)gpio_map;

}

int main(void) {
	
	setup_io();

	INP_GPIO(27);
	OUT_GPIO(27);

	int sockfd, newsockfd, portno;

	bool dateArriving;
	string date;


	socklen_t clilen;
	pthread_t t3;

	size_t cmd_size;
	
	struct sockaddr_in serv_addr, cli_addr;
	int n;
	
	sockfd = socket(AF_INET, SOCK_STREAM, 0);

	if ( sockfd < 0 ){
		error("ERROR opening the socket");
	}		

	bzero((char *)	 &serv_addr, sizeof(serv_addr));
	
	portno = 5002;

	//setup host_addr structure
	serv_addr.sin_family = AF_INET;

	//Fill the structure automatically with the host ip
	serv_addr.sin_addr.s_addr = INADDR_ANY;

	//convert port number to network byte order
	serv_addr.sin_port = htons(portno);	

	//Bind the socket to our ip and port
	if( bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) <0) {
		error("ERROR on binding");
	}

	//Listen for new connection , max queue = 1
	listen(sockfd,1);

	clilen = sizeof(cli_addr);

	cmd_size = sizeof(cmd);

	while(1){
		
		newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
		if ( newsockfd < 0){
			error("ERROR on accept");
		}

		printf("server: got connection from %s port %d\n", inet_ntoa(cli_addr.sin_addr), ntohs(cli_addr.sin_port));

		ip = inet_ntoa(cli_addr.sin_addr);

		usleep(100);

		video_start();

		while( !(read(newsockfd,cmd,cmd_size) < 0)){
			/*
			string str_cmd = string(cmd);			

			if( dateArriving && str_cmd.compare(1,1,"~") == 0 ){
				
				date = str_cmd.substr(2,14);
				write( newsockfd,"date-received",13 );
				dateArriving = false;
			}
	
			if( str_cmd.compare(1,4,"date") == 0 ){

				dateArriving = true;
				write( newsockfd,"date-ready",10 );

			}

			if( str_cmd.compare( 1,12,"record-start" ) == 0 && !isRecording ){
				
				isRecording = true;
				record_start(date);
				write( newsockfd,"record-started",14 );
			}

			if( str_cmd.compare( 1,11,"record-stop" ) == 0 && isRecording ){

				isRecording = false;
				record_stop();
				write( newsockfd,"record-stopped",14 );

			}			
			*/
			motorControl();

			usleep(50); //Less than 20 ticks per second

		}
	
		video_stop();

		close(newsockfd);
		
	}

	close(sockfd);


        return 0;

};//main

