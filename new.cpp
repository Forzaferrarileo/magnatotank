#include <syslog.h>
#include <signal.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "video.h"
#include "motor.h"

char cmd[15];

void error(const char *msg)
{
	perror(msg);
	exit(1);
}

int main(){

	Video vid;
	Motor mot;

	int sockfd, newsockfd, portno;
	socklen_t clilen;
	struct sockaddr_in serv_addr, cli_addr;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);

	if ( sockfd < 0 ){
		error("ERROR opening the socket");
	}

	bzero((char *)&serv_addr, sizeof(serv_addr));

	portno = 5002;

	//setup host_addr structure
	serv_addr.sin_family = AF_INET;

	//Fill the structure automatically with the host ip
	serv_addr.sin_addr.s_addr = INADDR_ANY;

	//convert port number to network byte order
	serv_addr.sin_port = htons(portno);	

	//Bind the socket to our ip and port
	if( bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) <0) {
		error("ERROR on binding");
	}

	//Listen for new connection , max queue = 1
	listen(sockfd,1);

	clilen = sizeof(cli_addr);

	int cmd_size = sizeof(cmd);

	for(;;){

		newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
		if ( newsockfd < 0){
			error("ERROR on accept");
		}

		printf("server: got connection from %s port %d\n", inet_ntoa(cli_addr.sin_addr), ntohs(cli_addr.sin_port));

		vid.hostIp = inet_ntoa(cli_addr.sin_addr);

		vid.UpdateSetup();

		vid.StartStream();

		while( !(read(newsockfd,cmd,cmd_size) < 0)){

			mot.motorUpdateCmd(cmd);

			usleep(1500000);

		}

		vid.StopStream();

		close(newsockfd);

	}

	close(sockfd);

	return 0;

}
