#include <string>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <linux/i2c-dev.h>
#include <fcntl.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>

class Motor{

public:
	Motor();
	//~Motor();
	int motorUpdateCmd(char[15]);
	
	int gunPin;
	char *i2cbus;
	
private:
	int i2c_write(int,int,int);

};
