#include "video.h"

Video::Video(){

	hostIp = "0.0.0.0";
	port = 5000;
	width = 1280;
	height = 720;
	framerate = 25;
	bitrate = 6000000;
	profile = "main";
	rtppayload = 96;
	config_interval = 1;

	pthread_create(&vidThread, NULL, &Video::videoThread, this);

}

void* Video::videoThread(void *ptr){

	Video *thiz;

	thiz = (Video *)ptr;

	gst_init(NULL, NULL);

	thiz->source = gst_element_factory_make("rpicamsrc","source");
	thiz->capsfilter = gst_element_factory_make("capsfilter","caps");
	thiz->rtppay = gst_element_factory_make("rtph264pay","rtph264");
	thiz->sink = gst_element_factory_make("udpsink","sink");

	thiz->UpdateSetup();

	thiz->pipeline = gst_pipeline_new("video_pipeline");
	gst_bin_add_many(GST_BIN(thiz->pipeline), thiz->source, thiz->capsfilter, thiz->rtppay, thiz->sink,NULL);

	gst_element_link_many(thiz->source, thiz->capsfilter, thiz->rtppay, thiz->sink,NULL);

	gst_element_set_state (thiz->pipeline, GST_STATE_READY);

	/* Wait until error or EOS */
	thiz->bus = gst_element_get_bus(thiz->pipeline);
	thiz->msg = gst_bus_timed_pop_filtered(thiz->bus, GST_CLOCK_TIME_NONE, GST_MESSAGE_ERROR );

	/* Free resources */
	if (thiz->msg != NULL){
		gst_message_unref(thiz->msg);
	}
	
	gst_object_unref(thiz->bus);
	gst_element_set_state (thiz->pipeline, GST_STATE_NULL);
	gst_object_unref(thiz->pipeline);

}

int Video::UpdateSetup(){ 	

    g_object_set(source, "bitrate", bitrate, NULL); 	
    g_object_set(rtppay, "config-interval", config_interval, NULL); 	
    g_object_set(rtppay, "pt" , rtppayload, NULL); 	
    g_object_set(sink, "host", hostIp.c_str() , NULL); 	
    g_object_set(sink, "port", port, NULL);

    g_object_set(capsfilter, "caps", gst_caps_new_simple("video/x-h264",
            "width", G_TYPE_INT, width,
            "height", G_TYPE_INT, height,
            "framerate", GST_TYPE_FRACTION, framerate, 1,
            "profile" ,G_TYPE_STRING, profile.c_str() ,NULL), NULL);

    return 0;

}

int Video::StartStream(){

	if( gst_element_set_state (pipeline, GST_STATE_PLAYING) == GST_STATE_CHANGE_SUCCESS )
	{
		return 0;
	}
	else
	{
		return -1;
	}

}

int Video::StopStream(){

	if( gst_element_set_state (pipeline, GST_STATE_PAUSED) == GST_STATE_CHANGE_SUCCESS )
	{
		return 0;
	}
	else
	{
		return -1;
	}

}

