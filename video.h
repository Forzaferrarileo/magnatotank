#include <string>
#include <gst/gst.h>
#include <pthread.h>

#ifndef VIDEO_H
#define VIDEO_H

	class Video {
	public:
		Video();
		int StartStream();
		int StopStream();
		int UpdateSetup();
		//Variables
		std::string hostIp;
		int port;
		int width;
		int height;
		int framerate;
		int bitrate;
		std::string profile;

		int rtppayload;
		int config_interval;

	private:
		//Gstreamer Elements
		GstElement *pipeline;
		GstElement *source;
		GstElement *capsfilter;
		GstElement *rtppay;
		GstElement *sink;
		//Gstreamer bus
		GstBus *bus;
		GstMessage *msg;

		//Video thread
		pthread_t vidThread;
		static void* videoThread(void *ptr);

	};

#endif
